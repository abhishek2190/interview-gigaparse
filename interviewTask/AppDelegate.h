//
//  AppDelegate.h
//  interviewTask
//
//  Created by mac2 on 10/25/16.
//  Copyright © 2016 GigaParse. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

